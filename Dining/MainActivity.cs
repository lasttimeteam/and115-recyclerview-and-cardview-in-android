﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Support.V7.Widget;
using System;

namespace Dining
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            var recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            recyclerView.SetLayoutManager(new LinearLayoutManager(this,
                                                                  LinearLayoutManager.Vertical,
                                                                  false));

            var adapter = new RestaurantAdapter(SampleData.GetRestaurants());
            adapter.ItemClick += OnItemClick;

            recyclerView.SetAdapter(adapter);
        }

        private void OnItemClick(object sender, int position)
        {
            System.Diagnostics.Debug.WriteLine("Click " + position);
        }
    }
}