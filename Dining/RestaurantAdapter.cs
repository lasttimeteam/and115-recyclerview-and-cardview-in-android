﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;

namespace Dining
{
    public class RestaurantAdapter : RecyclerView.Adapter
    {
        public event EventHandler<int> ItemClick;

        List<Restaurant> restaurants;
                
        public RestaurantAdapter(List<Restaurant> restaurants)
        {
            this.restaurants = restaurants;
        }

        public override int ItemCount => restaurants.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = (RestaurantViewHolder)holder;
            vh.Name.Text = restaurants[position].Name;
            vh.Rating.Rating = restaurants[position].Rating;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var layout = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.restaurant, parent, false);
            return new RestaurantViewHolder(layout, OnItemClick);
        }

        void OnItemClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }
    }
}
